var express = require("express");
var bodyParser = require('body-parser');


var app = express(); 

app.use(bodyParser());

const auction = require('./auction.js');


app.post('/', async function(req, resp){// addItemAuction

    let name = req.body.name;
    let desciption = req.body.desciption;
    let startTime = req.body.startTime;
    let endTime = req.body.endTime;
    let startAmount = req.body.startAmount;
    let winner = req.body.winner;  
    let imgUrl = req.body.imgUrl;
    let a = await auction.addItemAuction(name,desciption,startTime,endTime,startAmount,winner,imgUrl);
    console.log("my a",a);
    resp.send(a);


  });



app.post('/auctions', async function(req, resp){// list all items on auction, upcoming auctions, previous auctions.


  
    let a = await auction.auctionDetails();
    console.log("my a",a);
    resp.send(a);


  });


app.post('/itemDetails', async function(req, resp){// API to list details of an item.


    let itemId = req.body.itemId;
    let a = await auction.itemDetails(itemId);
    console.log("my a",a);
    resp.send(a);


  });

app.post('/submitBid', async function(req, resp){// API to submit a bid.


    let itemId = req.body.itemId;
    let userID = req.body.userID;
    let amount = req.body.amount;

    let a = await auction.submitBid(itemId, userID, amount);
    console.log("my a",a);
    resp.send(a);


  });

app.post('/viewBids', async function(req, resp){// API to view Bids submitted by an user.


   
    let userID = req.body.userID;

    let a = await auction.viewBids(userID);
    console.log("my a",a);
    resp.send(a);


  });



app.listen(5678, function(){

  console.log('listening on port 4321');

  });
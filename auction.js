var { commonQuery } = require('./db.js');


async function addItemAuction(name,desciption,startTime,endTime,startAmount,winner,imgUrl){


        return new Promise(async function(resolve, reject){
          
        let ins_q = "INSERT into sys.AuctionItem SET ?";

        let ins_params = {"itemNAme": name , "itemDescription": desciption, "startTime": startTime ,
         "endTime": endTime ,"startingAmount": startAmount, "winner": winner , "imgUrl": imgUrl };

         try{
        let ins_result = await commonQuery(ins_q,ins_params);
        resolve("Item inserted");
          }
          catch(err){
          	reject("item couldnt be inserted. Try again");
          }

        });


}

function getTime(){

 var d = new Date();
 var mydate = (
     d.getFullYear()  + "-" +
    ("00" + (d.getMonth() + 1)).slice(-2) + "-" + 
    ("00" + d.getDate()).slice(-2) + " "+
    
    ("00" + d.getHours()).slice(-2) + ":" + 
    ("00" + d.getMinutes()).slice(-2) + ":" + 
    ("00" + d.getSeconds()).slice(-2)
  );
 
 return (mydate);

}

async function auctionDetails(){

      let curTime = getTime();
      
       let read_query = "SELECT * FROM sys.AuctionItem where endTime < ? ";
       let read_by_param = [curTime];
       let read_q_res = await commonQuery(read_query, read_by_param);

       
       let read_query2 = "SELECT * FROM sys.AuctionItem where startTime > ? ";
       let read_by_param2 = [curTime];
       let read_q_res2 = await commonQuery(read_query2, read_by_param2);



       let read_query3 = "SELECT * FROM sys.AuctionItem where startTime < ?  AND endTime > ?";
       let read_by_param3 = [curTime, curTime];
       let read_q_res3 = await commonQuery(read_query3, read_by_param3);


       let myobj = {};
       myobj["old"] = read_q_res;
       myobj["future"] = read_q_res2;
       myobj["ongoing"] = read_q_res3;
       
       return myobj;


       //console.log("my res",myobj );
       
}

//auctionDetails();

async function itemDetails(itemId){
  
       let read_query = "SELECT * FROM sys.AuctionItem where `index` = ? ";
       let read_by_param = [itemId];
       let read_q_res = await commonQuery(read_query, read_by_param);

       
       if(new Date(getTime()) < read_q_res[0].endTime){ // ongoing auction

	       let read_query2 = "SELECT * FROM sys.Bids where item = ?"; //  
	       let read_by_param2 = [read_q_res[0].index];
	       let read_q_res2 = await commonQuery(read_query2, read_by_param2);

	       let myobj = {};
	       myobj["currentBidAmount"] = read_q_res2[0].amount;
	       console.log("myobj1", myobj);
	      // return myobj; 
         
       }
       else{ // auction completed
           
           let read_query3 = "SELECT * FROM sys.Bids where item = ?"; //  
	       let read_by_param3 = [read_q_res[0].index];
	       let read_q_res2 = await commonQuery(read_query3, read_by_param3);

	       let myobj = {};
	       myobj["BidAmount"] = read_q_res2[0].amount;
	       myobj["userId"] = read_q_res2[0].user;
	       console.log("myobj2", myobj);
	      // return myobj; 
        
         
       }

      
}

//itemDetails(5);

async function isLoggedIn(userID){
   
   return new Promise(async function(resolve, reject){

      	   let read_query = "SELECT * FROM sys.User where index = ?"; 
	       let read_by_param = [userID];
	       let read_q_res = await commonQuery(read_query, read_by_param);
	       if(read_q_res[0].isLoggedIn == 0){
	       	resolve(0);
	       }
	       else{
	       	resolve(1);
	       }
    

   })

}

function submitBid(itemId, userID, amount){

	     return new Promise(async function(resolve, reject){

        if(isLoggedIn(userID)){


        let ins_q = "INSERT into sys.Bids SET ?";

        let ins_params = {"item": itemId , "user": userID, "amount": amount};

         try{
        let ins_result = await commonQuery(ins_q,ins_params);
        resolve("Item inserted");
          }
          catch(err){
          	reject("item couldnt be inserted. Try again");
          }

         }
         else{
         	reject("Pls login to submit the bid" );
         } 

        });
   
 
}

function viewBids(userID){
     
     return new Promise(async function(resolve, reject){
        
           let read_query = "SELECT * FROM sys.Bids where user = ?"; //  
	       let read_by_param = [userID];
	       let read_q_res = await commonQuery(read_query, read_by_param);
           
           if(read_q_res.length > 0){
           	resolve(read_q_res);
           }
           else{
           	reject("No bids submitted by You!");
           }



     })

}






module.exports = {
  auctionDetails,
  addItemAuction,
  itemDetails,
  submitBid,
  viewBids

};